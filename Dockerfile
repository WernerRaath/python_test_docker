FROM python:3.6-alpine

COPY dockerize /usr/bin

RUN pip install pyyaml \
  && chmod +x /usr/bin/dockerize
